parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}
// LPARENTESES  ( 
// RPARENTESES  )

// LCURLY  {
// RCURLY  }

// LCHAVES [
// RCHAVES ]

program:  'class' 'Program' LCURLY (var_decl+)* method_decl* RCURLY EOF ;

method_decl: (type | TOKVOID ) IDENTIFICADOR LPARENTESES ((var_decl)+ PONTOS*)? RPARENTESES block;

block: LCURLY var_decl* statement* RCURLY;

var_decl: type (IDENTIFICADOR PONTOS* )* array_decl? PONTOS*;
array_decl: LCHAVES INTLITERAL RCHAVES;

type_id: type IDENTIFICADOR;

type: TOKINT | TOKBOOLEAN ;

statement: location assign_op expr PONTOS
					| method_call PONTOS 
					| TOKIF LPARENTESES expr RPARENTESES block ( TOKELSE block )? 
					| TOKFOR IDENTIFICADOR IGUAL expr PONTOS expr block 
					| TOKRETURN  expr? PONTOS 
					| TOKBREAK PONTOS 
					| TOKCONTINUE PONTOS
					| block ;

assign_op: IGUAL | MAISIUGUAL | MENOSIGUAL ;

method_call: IDENTIFICADOR LPARENTESES expr (PONTOS expr)* RPARENTESES
| TOKCALLOUT LPARENTESES STRINGLITERAL (PONTOS* ( expr | STRINGLITERAL)+ PONTOS*)? RPARENTESES ;

location: IDENTIFICADOR | IDENTIFICADOR LCHAVES expr RCHAVES ;

expr: location
		| method_call
		| identifier_desl
		| expr bin_op expr
		| bin_op expr*
		| PONTOS expr*
		| LPARENTESES expr* RPARENTESES ;

bin_op : arith_op 
		| rel_op 
		| eq_op 
		| cond_op ;

arith_op : MAIS | MENOS | VEZES | DIVISAO | PORCENTO ;
rel_op: MENOR| MAIOR| MENORIGUAL| MAIORIGUAL ;
eq_op: COMPARACAO | DIFERENTE ;
cond_op: AND | OU ;

identifier_desl: int_literal int_literal* | int_literal | char_literal | bool_literal ;

int_literal: INTLITERAL;
char_literal: CHAR ;
bool_literal : TOKTRUE | TOKFALSE ;