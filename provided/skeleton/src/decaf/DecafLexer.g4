lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

LPARENTESES : '(';
RPARENTESES: ')';

LCURLY: '{';
RCURLY : '}';

LCHAVES : '[';
RCHAVES : ']';

TOKPROGAM: 'Program';
TOKIF: 'if';
TOKBOOLEAN : 'boolean';
TOKCALLOUT : 'callout';
TOKCLASS : 'class';
TOKELSE : 'else';
TOKINT : 'int';
TOKRETURN : 'return';
TOKVOID: 'void';
TOKFOR : 'for';
TOKBREAK : 'break';
TOKCONTINUE : 'continue';

TOKTRUE : 'true';
TOKFALSE : 'false';


MAIS: '+';
MENOS: '-';
VEZES: '*';
DIVISAO: '/';
PORCENTO: '%';

MENOR: '<';
MAIOR: '>';
MENORIGUAL: '<=';
MAIORIGUAL: '>=';

COMPARACAO: '==';
DIFERENTE: '!=';

AND: '&&';
OU: '||';

MAISIUGUAL: '+=';
MENOSIGUAL: '-='; 

IGUAL: '=';

PONTOS: ('!'|';'|',');


IDENTIFICADOR: ('a'..'z' | 'A'..'Z' | '_' | '%' )+  ([0-9])* IDENTIFICADOR?;

INTLITERAL: [0-9]+ ('x' ([a-fA-F] | [0-9])+)?;

DECLVAR: [1-9]+;

WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

CHAR : '\'' ( [ !#-&(-.0-Z^-~] | INTEIRO | ESC) '\'';

STRINGLITERAL : '"' (IDENTIFICADOR | PONTUACAO)+ '"';

fragment INTEIRO: [0-9];
fragment PONTUACAO: ( '.' | '?' | ',' | ';' | ' ' | ':'| '!' | ESPECIAL);
fragment ESPECIAL: '\\' ( '\'' | '\"' | '\\' | IDENTIFICADOR );
fragment ESC :  '\\' ('n'|'t'|'\\'|'"');